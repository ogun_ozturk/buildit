package com.esi.buildit.auth.rest;

import com.esi.buildit.auth.application.dto.AuthenticationRequest;
import com.esi.buildit.auth.application.dto.AuthenticationResponse;
import com.esi.buildit.auth.domain.model.BuildItUser;
import com.esi.buildit.auth.domain.repository.BuildItUserRepository;
import com.esi.buildit.auth.security.JwtTokenProvider;
import com.esi.buildit.common.application.exception.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.ResponseEntity.ok;

@RequestMapping("api/v1/auth")
@RestController
public class AuthController {

    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final BuildItUserRepository buildItUserRepository;

    public AuthController(AuthenticationManager authenticationManager,
                          JwtTokenProvider jwtTokenProvider,
                          BuildItUserRepository buildItUserRepository) {

        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.buildItUserRepository = buildItUserRepository;
    }

    @CrossOrigin
    @PostMapping("/sign-in")
    public ResponseEntity signIn(@RequestBody AuthenticationRequest authenticationRequest) {

        try {
            String username = authenticationRequest.getUsername();
            String password = authenticationRequest.getPassword();

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

            Optional<BuildItUser> optionalBuildItUser = buildItUserRepository.findByUsername(username);

            if (!optionalBuildItUser.isPresent()) {
                throw new UsernameNotFoundException("Username " + username + "not found"); //TODO handle error
            }

            BuildItUser buildItUser = optionalBuildItUser.get();
            String role = buildItUser.getRole().getRoleType().name();

            String token = jwtTokenProvider.createToken(
                    username,
                    buildItUser.getRole().getRoleType().name());

            return ok(AuthenticationResponse.of(
                    username,
                    token,
                    role)
            );
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username/password supplied"); //TODO handle error
        }
    }

    @CrossOrigin
    @GetMapping("/current-user-information")
    public ResponseEntity currentUser(@AuthenticationPrincipal UserDetails userDetails) {
        Map<Object, Object> model = new HashMap<>();

        if (userDetails == null) {
            throw new NotFoundException("User has not found");
        }

        model.put("username", userDetails.getUsername());
        model.put("roles", userDetails.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(toList())
        );

        return ok(model);
    }
}