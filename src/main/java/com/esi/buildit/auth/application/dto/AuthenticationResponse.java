package com.esi.buildit.auth.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
public class AuthenticationResponse {
    private String username;
    private String token;
    private String role;
}
