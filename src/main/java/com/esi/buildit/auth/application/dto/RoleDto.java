package com.esi.buildit.auth.application.dto;

import com.esi.buildit.auth.domain.model.RoleType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
public class RoleDto {
    private Long _id;
    private RoleType roleType;
    private String description;
}
