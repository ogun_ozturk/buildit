package com.esi.buildit.auth.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
public class BuildItUserDto {
    private Long _id;
    private String firstName;
    private String lastName;
    private String userName;
}
