package com.esi.buildit.auth.application.service;

import com.esi.buildit.auth.domain.repository.BuildItUserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service(value = "buildItUserService")
public class BuildItUserService implements UserDetailsService {

    private BuildItUserRepository buildItUserRepository;

    public BuildItUserService(BuildItUserRepository buildItUserRepository) {
        this.buildItUserRepository = buildItUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return buildItUserRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username: " + username + " not found"));
    }
}