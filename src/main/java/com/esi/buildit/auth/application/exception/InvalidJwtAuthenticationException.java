package com.esi.buildit.auth.application.exception;

import org.springframework.security.core.AuthenticationException;

public class InvalidJwtAuthenticationException extends AuthenticationException {

    private String userMessage;

    public InvalidJwtAuthenticationException(String errorMessage, String userMessage) {
        super(errorMessage);
        this.userMessage = userMessage;
    }

    public String getUserMessage() {
        return userMessage;
    }
}
