package com.esi.buildit.auth.config;

import com.esi.buildit.auth.security.JwtConfigurer;
import com.esi.buildit.auth.security.JwtTokenProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String ROLE_SITE_ENGINEER = "SITE_ENGINEER";
    private static final String ROLE_WORKS_ENGINEER = "WORKS_ENGINEER";

    private final JwtTokenProvider jwtTokenProvider;
    private final FilterChainExceptionHandler filterChainExceptionHandler;

    public SecurityConfig(JwtTokenProvider jwtTokenProvider, FilterChainExceptionHandler filterChainExceptionHandler) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.filterChainExceptionHandler = filterChainExceptionHandler;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

//        http
//                .authorizeRequests()
//                //.antMatchers("/api/**")
//                .antMatchers("/**")
//                .anonymous()
//                .anyRequest()
//                .authenticated()
//                .and().csrf().disable().formLogin().disable();

        http
                .httpBasic()
                .disable()
                .csrf()
                .disable()
                .cors()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/api/v1/auth/sign-in").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/invoice/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/api/v1/invoice/{id}/accept").permitAll()
                .antMatchers(HttpMethod.PUT, "/api/v1/invoice/{id}/reject").permitAll()
                .antMatchers(HttpMethod.PUT, "/api/v1/phr/{id}/accept").permitAll()
                .antMatchers(HttpMethod.PUT, "/api/v1/phr/{id}/reject").permitAll()
                .antMatchers(HttpMethod.PUT, "/api/v1/phr/{id}/dispatch-review").permitAll()
                .antMatchers(HttpMethod.PUT, "/api/v1/invoice/{id}/submit-remittance-advice").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/phr/{id}").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/phr/pending").hasRole(ROLE_WORKS_ENGINEER)
                .antMatchers(HttpMethod.GET, "/api/v1/phr").hasAnyRole(ROLE_SITE_ENGINEER, ROLE_WORKS_ENGINEER)
                .antMatchers(HttpMethod.POST, "/api/v1/phr").hasRole(ROLE_SITE_ENGINEER)
                .antMatchers(HttpMethod.PUT, "/api/v1/phr/{id}").hasAnyRole(ROLE_SITE_ENGINEER, ROLE_WORKS_ENGINEER)
                .antMatchers(HttpMethod.PUT, "/api/v1/phr/{id}/cancel").hasRole(ROLE_SITE_ENGINEER)
                .antMatchers(HttpMethod.GET, "/api/v1/phr/{id}/comment").hasRole(ROLE_WORKS_ENGINEER)
                .antMatchers(HttpMethod.POST, "/api/v1/phr/{id}/comment").hasRole(ROLE_WORKS_ENGINEER)
                .antMatchers(HttpMethod.POST, "/api/v1/plant-supplier/{id}/urls").hasRole(ROLE_SITE_ENGINEER)
                .antMatchers(HttpMethod.POST, "/api/v1/plant-supplier/customers").hasAnyRole(ROLE_SITE_ENGINEER, ROLE_WORKS_ENGINEER)
                .antMatchers(HttpMethod.POST, "/api/v1/construction-site").hasRole(ROLE_SITE_ENGINEER)
                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider, filterChainExceptionHandler));
    }
}
