package com.esi.buildit.auth.domain.repository;

import com.esi.buildit.auth.domain.model.BuildItUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BuildItUserRepository extends JpaRepository<BuildItUser, Long> {
    Optional<BuildItUser> findByUsername(String username);
}
