package com.esi.buildit.auth.domain.repository;

import com.esi.buildit.auth.domain.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
