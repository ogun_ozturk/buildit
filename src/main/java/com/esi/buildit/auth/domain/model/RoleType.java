package com.esi.buildit.auth.domain.model;

public enum RoleType {
    ROLE_SITE_ENGINEER,
    ROLE_WORKS_ENGINEER
}
