package com.esi.buildit.phr.domain.repository;

import com.esi.buildit.phr.domain.model.PhrComment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PhrCommentRepository extends JpaRepository<PhrComment, Long> {
    List<PhrComment> findPhrCommentsByPlantHireRequest_Id(Long phrId);
}
