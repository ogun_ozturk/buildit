package com.esi.buildit.phr.domain.model;

import com.esi.buildit.auth.domain.model.BuildItUser;
import com.esi.buildit.common.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
@Entity
public class PhrComment extends BaseEntity {

    @Column(nullable = false)
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plant_hire_request_id")
    private PlantHireRequest plantHireRequest;

    @ManyToOne
    @JoinColumn(name = "works_engineer_id", nullable = false)
    private BuildItUser worksEngineer;
}
