package com.esi.buildit.phr.domain.repository;

import com.esi.buildit.phr.domain.model.PhrStatus;
import com.esi.buildit.phr.domain.model.PlantHireRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PhrRepository extends JpaRepository<PlantHireRequest, Long> {
    List<PlantHireRequest> findPlantHireRequestsByStatusEquals(PhrStatus status);
    Optional<PlantHireRequest> findByPurchaseOrderUuid(UUID uuid);
}
