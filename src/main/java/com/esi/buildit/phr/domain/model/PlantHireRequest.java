package com.esi.buildit.phr.domain.model;

import com.esi.buildit.auth.domain.model.BuildItUser;
import com.esi.buildit.common.domain.BaseEntity;
import com.esi.buildit.common.domain.BusinessPeriod;
import com.esi.buildit.constructionsite.domain.model.ConstructionSite;
import com.esi.buildit.plantsupplier.domain.model.PlantSupplier;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
@Entity
@Table(name = "plant_hire_request", indexes = {@Index(name = "po_idx", columnList = "po_uuid")})
public class PlantHireRequest extends BaseEntity {

    @Column(name = "po_uuid")
    private UUID purchaseOrderUuid;

    @ManyToOne
    @JoinColumn(name = "construction_site_id", nullable = false)
    private ConstructionSite constructionSite;

    @ManyToOne
    @JoinColumn(name = "plant_supplier_id", nullable = false)
    private PlantSupplier plantSupplier;

    @Column(nullable = false)
    private Long plantItemIdentifier;

    @Embedded
    @Column(nullable = false)
    private BusinessPeriod rentalPeriod;

    @Column(precision = 8, scale = 2, nullable = false)
    private BigDecimal totalCost;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private PhrStatus status;

    @ManyToOne
    @JoinColumn(name = "site_engineer_id", nullable = false)
    private BuildItUser siteEngineer;

    private String resourceLocation;

    @OneToMany(mappedBy = "plantHireRequest", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<PhrComment> phrComments;

    public static PlantHireRequest of(ConstructionSite constructionSite,
                                      PlantSupplier plantSupplier,
                                      Long plantItemIdentifier,
                                      BusinessPeriod rentalPeriod,
                                      BigDecimal cost,
                                      BuildItUser siteEngineer) {

        PlantHireRequest plantHireRequest = new PlantHireRequest();

        plantHireRequest.constructionSite = constructionSite;
        plantHireRequest.plantSupplier = plantSupplier;
        plantHireRequest.plantItemIdentifier = plantItemIdentifier;
        plantHireRequest.rentalPeriod = rentalPeriod;
        plantHireRequest.totalCost = cost;
        plantHireRequest.status = PhrStatus.PENDING_FOR_WORKS_ENGINEER_APPROVAL;
        plantHireRequest.siteEngineer = siteEngineer;

        return plantHireRequest;
    }
}
