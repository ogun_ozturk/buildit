package com.esi.buildit.phr.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidUpdateRequestException extends RuntimeException {
    public InvalidUpdateRequestException(String message) {
        super(message);
    }
}
