package com.esi.buildit.phr.application.service;

import com.esi.buildit.auth.domain.model.BuildItUser;
import com.esi.buildit.auth.domain.repository.BuildItUserRepository;
import com.esi.buildit.common.application.dto.BusinessPeriodDto;
import com.esi.buildit.common.application.exception.InvalidBusinessPeriodException;
import com.esi.buildit.common.application.exception.NotFoundException;
import com.esi.buildit.common.application.exception.RentItException;
import com.esi.buildit.common.domain.BusinessPeriod;
import com.esi.buildit.constructionsite.domain.model.ConstructionSite;
import com.esi.buildit.constructionsite.domain.repository.ConstructionSiteRepository;
import com.esi.buildit.phr.application.dto.*;
import com.esi.buildit.phr.application.exception.InvalidUpdateRequestException;
import com.esi.buildit.phr.domain.model.PhrComment;
import com.esi.buildit.phr.domain.model.PhrStatus;
import com.esi.buildit.phr.domain.model.PlantHireRequest;
import com.esi.buildit.phr.domain.repository.PhrCommentRepository;
import com.esi.buildit.phr.domain.repository.PhrRepository;
import com.esi.buildit.plantsupplier.domain.model.PlantSupplier;
import com.esi.buildit.plantsupplier.domain.repository.PlantSupplierRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.esi.buildit.common.application.service.BusinessPeriodValidator.validateBusinessPeriod;
import static com.esi.buildit.phr.domain.model.PhrStatus.*;

// TODO refactor repeating lines for error throwing and sending request
@Service
public class PhrService {

    private final PhrRepository phrRepository;
    private final PlantSupplierRepository plantSupplierRepository;
    private final ConstructionSiteRepository constructionSiteRepository;
    private final BuildItUserRepository buildItUserRepository;
    private final PhrCommentRepository phrCommentRepository;

    private final PhrAssembler phrAssembler;

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    private final ModelMapper modelMapper;

    private static final Long rentITcustomerID = 1L;

    public PhrService(PhrRepository phrRepository,
                      PlantSupplierRepository plantSupplierRepository,
                      ConstructionSiteRepository constructionSiteRepository,
                      BuildItUserRepository buildItUserRepository,
                      PhrCommentRepository phrCommentRepository,
                      PhrAssembler phrAssembler,
                      RestTemplate restTemplate,
                      ObjectMapper objectMapper,
                      ModelMapper modelMapper) {

        this.phrRepository = phrRepository;
        this.plantSupplierRepository = plantSupplierRepository;
        this.constructionSiteRepository = constructionSiteRepository;
        this.buildItUserRepository = buildItUserRepository;
        this.phrCommentRepository = phrCommentRepository;
        this.phrAssembler = phrAssembler;
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
        this.modelMapper = modelMapper;
    }

    public List<PhrDto> getPHRs() {
        List<PlantHireRequest> phrs = phrRepository.findAll();
        if (phrs.isEmpty()) throw new NotFoundException("There are no plant hiring requests yet");
        return phrAssembler.toResources(phrs);

    }

    public PhrDto getPHRById(Long phrId) {
        return phrAssembler.toResource(getPlantHiringRequestById(phrId));
    }

    public PhrDto createPHR(PhrDto phrDto, String username) {

        BusinessPeriod rentalPeriod = BusinessPeriod.of(
                phrDto.getRentalPeriod().getStartDate(),
                phrDto.getRentalPeriod().getEndDate()
        );

        validateBusinessPeriod(rentalPeriod);

        PlantHireRequest phr = PlantHireRequest.of(
                getConstructionSiteById(phrDto.getConstructionSiteId()),
                getPlantSupplierById(phrDto.getPlantSupplierId()),
                phrDto.getPlantInventoryItemId(),
                rentalPeriod,
                phrDto.getTotalPrice(),
                getBuildItUserByUsername(username)
        );

        phrRepository.save(phr);

        return phrAssembler.toResource(phr);
    }

    //TODO update based on role, refactor updatable fields
    public PhrDto updatePHR(Long phrId, PhrDto phrDto) {

        PlantHireRequest phr = getPlantHiringRequestById(phrId);

        BusinessPeriod rentalPeriod = BusinessPeriod.of(
                phrDto.getRentalPeriod().getStartDate(),
                phrDto.getRentalPeriod().getEndDate()
        );

        ResponseEntity<PhrDto> response = null;
        PhrDto dto = null;
        BusinessPeriod current = phr.getRentalPeriod();
        PhrStatus status = phr.getStatus();
        switch (status) {
            case PENDING_FOR_WORKS_ENGINEER_APPROVAL:
                // modifying construction site
                Long constrId = phrDto.getConstructionSiteId();
                if (constrId != null) {
                    Optional<ConstructionSite> constrOpt = constructionSiteRepository.findById(constrId);
                    if (!constrOpt.isPresent()) throw new NotFoundException("Modification failed. Cannot find such construction site");
                    phr.setConstructionSite(constrOpt.get());
                }

                // modifying rental period
                boolean startDatePostponed = rentalPeriod.getStartDate().isAfter(current.getStartDate());
                boolean endDateBroughtForward = rentalPeriod.getEndDate().isBefore(current.getEndDate());
                if (startDatePostponed && endDateBroughtForward) phr.setRentalPeriod(rentalPeriod);
                phrRepository.save(phr);
                break;

            case IN_USE:
                response = makeRentITRequest(null, phr.getResourceLocation(), HttpMethod.GET);
                dto = response.getBody();
                if (response.getStatusCode() != HttpStatus.OK || dto == null)
                    throw new RentItException("Such PO was not found from RentIT system", response.getStatusCode());
                Link modify = dto.getLink("modify");
                if (modify == null) throw new RentItException("RentIT does not allow to update PO currently", HttpStatus.UNPROCESSABLE_ENTITY);

                // The PO was found (extending the rental period)
                if (rentalPeriod.getEndDate().isAfter(current.getEndDate())) {
                    dto = phrAssembler.toResource(phr);
                    BusinessPeriodDto currentDto = BusinessPeriodDto.of(current.getStartDate(), rentalPeriod.getEndDate());
                    dto.setRentalPeriod(currentDto);
                    dto.setRentITCustomerId(rentITcustomerID);
                    response = makeRentITRequest(dto, modify.getHref(), HttpMethod.PUT);
                    dto = response.getBody();
                    if (response.getStatusCode() != HttpStatus.OK || dto == null) throw new RentItException("Extending rental period was unsuccessful in rentIT", response.getStatusCode());

                    // Modification is now pending in rentIT side
                    phr.setStatus(EXTENTION_REQUESTED);
                    phrRepository.save(phr);
                }
                break;
        }
        return phrAssembler.toResource(phr);
    }

    public List<PhrDto> getPendingPHRs() {
        List<PlantHireRequest> pendingPHRs = phrRepository.findPlantHireRequestsByStatusEquals(PENDING_FOR_WORKS_ENGINEER_APPROVAL);

        if (pendingPHRs == null || pendingPHRs.isEmpty()) {
            throw new NotFoundException("There are no pending plant hiring requests");
        }
        return phrAssembler.toResources(pendingPHRs);
    }

    public PhrDto acceptPHR(Long phrId) {

        PlantHireRequest phr = getPlantHiringRequestById(phrId);

        ResponseEntity<PhrDto> response = null;
        PhrDto dto = null;
        PhrStatus status = phr.getStatus();
        switch (status) {
            case PENDING_FOR_WORKS_ENGINEER_APPROVAL:
                dto = phrAssembler.toResource(phr);
                dto.setRentITCustomerId(rentITcustomerID);
                response = makeRentITRequest(dto, phr.getPlantSupplier().getPoUrl(), HttpMethod.POST);
                dto = response.getBody();
                if (response.getStatusCode() != HttpStatus.CREATED || dto == null)
                    throw new RentItException("Cannot create PO in RentIT system", response.getStatusCode());
                // successfully created PO in rentIT
                phr.setStatus(APPROVED_BY_WORKS_ENGINEER);
                UUID uuid = UUID.fromString(dto.getPurchaseOrderUuid());
                phr.setPurchaseOrderUuid(uuid);

                Link self = dto.getLink("self");
                if (self == null) throw new RentItException("No self href specified in links of PO", response.getStatusCode());
                phr.setResourceLocation(self.getHref());
                phrRepository.save(phr);
                break;
            case BEING_REVIEWED:
                response = makeRentITRequest(null, phr.getResourceLocation(), HttpMethod.GET);
                dto = response.getBody();
                if (response.getStatusCode() != HttpStatus.OK || dto == null)
                    throw new RentItException("Such PO was not found from RentIT system", response.getStatusCode());
                // successfully found corresponding PO in rentIT
                Link accept = dto.getLink("accept");
                if (accept == null) throw new RentItException("Accept is not available for PO in rentIT", response.getStatusCode());

                response = makeRentITRequest(null, accept.getHref(), HttpMethod.PUT);
                if (response.getStatusCode() != HttpStatus.OK)
                    throw new RentItException("Error in rentIT system while accepting dispatch review", response.getStatusCode());
                // successfully accepted dispatch review
                phr.setStatus(IN_USE);
                phrRepository.save(phr);
                break;
            case EXTENTION_REQUESTED:
                response = makeRentITRequest(null, phr.getResourceLocation(), HttpMethod.GET);
                dto = response.getBody();
                if (response.getStatusCode() != HttpStatus.OK || dto == null)
                    throw new RentItException("Such PO was not found from RentIT system", response.getStatusCode());
                // successfully found corresponding PO in rentIT
                phr.setTotalCost(dto.getTotalPrice());
                BusinessPeriod period = BusinessPeriod.of(dto.getRentalPeriod().getStartDate(), dto.getRentalPeriod().getEndDate());
                phr.setRentalPeriod(period);
                phr.setStatus(IN_USE);
                phrRepository.save(phr);
                break;
        }
        return phrAssembler.toResource(phr);
    }

    public PhrDto rejectPHR(Long phrId) {

        PlantHireRequest phr = getPlantHiringRequestById(phrId);

        PhrDto dto = null;
        PhrStatus status = phr.getStatus();
        switch (status) {
            case PENDING_FOR_WORKS_ENGINEER_APPROVAL:
                phr.setStatus(REJECTED_BY_WORKS_ENGINEER);
                phrRepository.save(phr);
                break;
            case BEING_REVIEWED:
                ResponseEntity<PhrDto> response = makeRentITRequest(null, phr.getResourceLocation(), HttpMethod.GET);
                dto = response.getBody();
                if (response.getStatusCode() != HttpStatus.OK || dto == null)
                    throw new RentItException("Such PO was not found from RentIT system", response.getStatusCode());
                // successfully found corresponding PO in rentIT
                Link reject = dto.getLink("reject");
                if (reject == null) throw new RentItException("Reject is not available for PO in rentIT", response.getStatusCode());

                response = makeRentITRequest(null, reject.getHref(), HttpMethod.PUT);
                if (response.getStatusCode() != HttpStatus.OK)
                    throw new RentItException("Error in rentIT system while rejecting dispatch review", response.getStatusCode());
                // successfully rejected dispatch review
                phr.setStatus(CANCELLED);
                phrRepository.save(phr);
                break;
            case EXTENTION_REQUESTED:
                phr.setStatus(IN_USE);
                phrRepository.save(phr);
        }
        return phrAssembler.toResource(phr);
    }

    public PhrDto dispatchReview(Long id) {
        PlantHireRequest phr = getPlantHiringRequestById(id);
        if (phr.getStatus() == APPROVED_BY_WORKS_ENGINEER) {
            phr.setStatus(BEING_REVIEWED);
            phrRepository.save(phr);
        }
        return phrAssembler.toResource(phr);
    }

    // TODO move date calculation logic to RentIT
    public PhrDto cancelPHR(Long phrId) {

        PlantHireRequest phr = getPlantHiringRequestById(phrId);

        PhrDto dto = null;
        PhrStatus status = phr.getStatus();
        switch (status) {
            case PENDING_FOR_WORKS_ENGINEER_APPROVAL:
                phr.setStatus(CANCELLED);
                phrRepository.save(phr);
                break;
            case APPROVED_BY_WORKS_ENGINEER:
                ResponseEntity<PhrDto> response = makeRentITRequest(null, phr.getResourceLocation(), HttpMethod.GET);
                dto = response.getBody();
                if (response.getStatusCode() != HttpStatus.OK || dto == null)
                    throw new RentItException("Such PO was not found from RentIT system", response.getStatusCode());
                // successfully found corresponding PO in rentIT
                Link cancel = dto.getLink("cancel");
                if (cancel == null) throw new RentItException("Cancel is not available for PO in rentIT", response.getStatusCode());

                long daysBetween = ChronoUnit.DAYS.between(LocalDate.now(), dto.getShipmentDate());
                if (daysBetween < 2) throw new RentItException("Plant is going to be dispatched, cancellation is not allowed", response.getStatusCode());

                response = makeRentITRequest(null, cancel.getHref(), HttpMethod.PUT);
                if (response.getStatusCode() != HttpStatus.OK)
                    throw new RentItException("Error in rentIT system while cancelling the PO in RentIT system", response.getStatusCode());
                // successfully cancelled PO in rentIT system
                phr.setStatus(CANCELLED);
                phrRepository.save(phr);
                break;
        }
        return phrAssembler.toResource(phr);
    }

    public List<PhrCommentDto> getPhrCommentsByPhrId(Long phrId) {
        List<PhrComment> phrComments = phrCommentRepository.findPhrCommentsByPlantHireRequest_Id(phrId);

        if (phrComments == null || phrComments.isEmpty()) {
            throw new NotFoundException("There are no comments for this plant hiring request");
        }

        return phrComments.stream()
                .map(c -> PhrCommentDto.of(c.getId(), c.getComment()))
                .collect(Collectors.toList());
    }

    // TODO get engineer from auth user not dto
    public PhrCommentDto addPhrComment(Long phrId, PhrCommentDto phrCommentDto, String username) {
        PlantHireRequest phr = getPlantHiringRequestById(phrId);
        BuildItUser worksEngineer = getBuildItUserByUsername(username);

        PhrComment phrComment = PhrComment.of(phrCommentDto.getComment(), phr, worksEngineer);
        phrCommentRepository.save(phrComment);
        return PhrCommentDto.of(phrComment.getId(), phrComment.getComment());
    }

    private PlantHireRequest getPlantHiringRequestById(Long phrId) {
        Optional<PlantHireRequest> optionalPhr = phrRepository.findById(phrId);

        if (!optionalPhr.isPresent()) {
            throw new NotFoundException("Plant hiring request has not found");
        }

        return optionalPhr.get();
    }

    private ConstructionSite getConstructionSiteById(Long constructionSiteId) {
        Optional<ConstructionSite> optionalConstructionSite = constructionSiteRepository.findById(constructionSiteId);

        if (!optionalConstructionSite.isPresent()) {
            throw new NotFoundException("Construction site has not found");
        }

        return optionalConstructionSite.get();
    }

    private PlantSupplier getPlantSupplierById(Long plantSupplierId) {
        Optional<PlantSupplier> optionalPlantSupplier = plantSupplierRepository.findById(plantSupplierId);

        if (!optionalPlantSupplier.isPresent()) {
            throw new NotFoundException("Plant supplier site has not found");
        }

        return optionalPlantSupplier.get();
    }

    private BuildItUser getBuildItUserByUsername(String username) {
        Optional<BuildItUser> optionalBuildItUser = buildItUserRepository.findByUsername(username);

        if (!optionalBuildItUser.isPresent()) {
            throw new NotFoundException("Engineer has not found");
        }

        return optionalBuildItUser.get();
    }

    private ResponseEntity<PhrDto> makeRentITRequest(PhrDto dto, String url, HttpMethod method) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaTypes.HAL_JSON_UTF8);
        return restTemplate.exchange(url, method, new HttpEntity<>(dto, headers), PhrDto.class);
    }
}
