package com.esi.buildit.phr.application.service;

import com.esi.buildit.common.application.dto.BusinessPeriodDto;
import com.esi.buildit.common.domain.BusinessPeriod;
import com.esi.buildit.invoice.rest.InvoiceController;
import com.esi.buildit.phr.application.dto.PhrCommentDto;
import com.esi.buildit.phr.application.dto.PhrDto;
import com.esi.buildit.phr.domain.model.PhrComment;
import com.esi.buildit.phr.domain.model.PhrStatus;
import com.esi.buildit.phr.domain.model.PlantHireRequest;
import com.esi.buildit.phr.rest.PhrRestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class PhrAssembler extends ResourceAssemblerSupport<PlantHireRequest, PhrDto> {

    public PhrAssembler() {
        super(PhrRestController.class, PhrDto.class);
    }

    @Override
    public PhrDto toResource(PlantHireRequest phr) {
        PhrDto dto = createResourceWithId(phr.getId(), phr);

        dto.set_id(phr.getId());
        UUID uuid = phr.getPurchaseOrderUuid();
        if (uuid != null) dto.setPurchaseOrderUuid(uuid.toString());
        dto.setConstructionSiteId(phr.getConstructionSite().getId());
        dto.setConstructionSiteName(phr.getConstructionSite().getName());
        dto.setPlantSupplierId(phr.getPlantSupplier().getId());
        dto.setPlantSupplierName(phr.getPlantSupplier().getName());
        dto.setPlantInventoryItemId(phr.getPlantItemIdentifier());
        BusinessPeriod p = phr.getRentalPeriod();
        dto.setRentalPeriod(BusinessPeriodDto.of(p.getStartDate(), p.getEndDate()));
        dto.setTotalPrice(phr.getTotalCost());
        dto.setStatus(phr.getStatus().name());
        dto.setSiteEngineerId(phr.getSiteEngineer().getId());

        if (phr.getPhrComments() != null) {
            List<PhrCommentDto> commentsDtos = phr
                    .getPhrComments()
                    .stream()
                    .map(c -> PhrCommentDto.of(c.getId(), c.getComment()))
                    .collect(Collectors.toList());
            dto.setPhrCommentDtoList(commentsDtos);
        }

        // TODO: add links
        PhrStatus status = phr.getStatus();
        switch (status) {
            case PENDING_FOR_WORKS_ENGINEER_APPROVAL:
                dto.add(linkTo(methodOn(PhrRestController.class).accept(dto.get_id())).withRel("accept"));
                dto.add(linkTo(methodOn(PhrRestController.class).reject(dto.get_id())).withRel("reject"));
                dto.add(linkTo(methodOn(PhrRestController.class).cancel(dto.get_id())).withRel("cancel"));
                dto.add(linkTo(methodOn(PhrRestController.class).updatePhr(dto.get_id(), null)).withRel("modify"));
                dto.add(linkTo(methodOn(PhrRestController.class).addPhrComment(dto.get_id(), null, null)).withRel("comment"));
                break;
            case APPROVED_BY_WORKS_ENGINEER:
                dto.add(linkTo(methodOn(PhrRestController.class).cancel(dto.get_id())).withRel("cancel"));
                dto.add(linkTo(methodOn(PhrRestController.class).dispatchReview(dto.get_id())).withRel("dispatch-review"));
                break;
            case REJECTED_BY_WORKS_ENGINEER:
                // no operations
                break;
            case BEING_REVIEWED:
                dto.add(linkTo(methodOn(PhrRestController.class).accept(dto.get_id())).withRel("accept"));
                dto.add(linkTo(methodOn(PhrRestController.class).reject(dto.get_id())).withRel("reject"));
                break;
            case IN_USE:
                dto.add(linkTo(methodOn(PhrRestController.class).updatePhr(dto.get_id(), null)).withRel("modify"));
                break;
            case EXTENTION_REQUESTED:
                dto.add(linkTo(methodOn(PhrRestController.class).accept(dto.get_id())).withRel("accept"));
                dto.add(linkTo(methodOn(PhrRestController.class).reject(dto.get_id())).withRel("reject"));
                break;
            case INVOICED_BY_SUPPLIER:
                // no operation
                break;
        }

        return dto;
    }
}
