package com.esi.buildit.phr.application.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
@Data
public class TotalPriceResponse {
    private Long plantInventoryItemId;
    private LocalDate startDate;
    private LocalDate endDate;
    private Long hiringDays;
    private BigDecimal pricePerDay;
    private BigDecimal totalPrice;
}
