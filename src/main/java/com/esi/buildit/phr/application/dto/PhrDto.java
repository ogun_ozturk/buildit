package com.esi.buildit.phr.application.dto;

import com.esi.buildit.common.application.dto.BusinessPeriodDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
public class PhrDto extends ResourceSupport {

    private Long _id;
    private Long rentITCustomerId;
    private String purchaseOrderUuid;
    private Long constructionSiteId;
    private String constructionSiteName;
    private Long plantSupplierId;
    private String plantSupplierName;
    private Long plantInventoryItemId;
    private BusinessPeriodDto rentalPeriod;
    private BigDecimal totalPrice;
    private String status;
    private Long siteEngineerId;
    private LocalDate shipmentDate;
    private List<PhrCommentDto> phrCommentDtoList;
}
