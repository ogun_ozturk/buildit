package com.esi.buildit.phr.rest;

import com.esi.buildit.phr.application.dto.PhrCommentDto;
import com.esi.buildit.phr.application.dto.PhrDto;
import com.esi.buildit.phr.application.service.PhrService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/phr")
public class PhrRestController {

    private final PhrService phrService;

    @Autowired
    public PhrRestController(PhrService phrService) {
        this.phrService = phrService;
    }

    @CrossOrigin
    @GetMapping
    public ResponseEntity<List<PhrDto>> getPHRs() {
        return new ResponseEntity<>(phrService.getPHRs(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<PhrDto> getPHRbyId(@PathVariable Long id) {
        return new ResponseEntity<>(phrService.getPHRById(id), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping
    public ResponseEntity<PhrDto> createPhr(@RequestBody PhrDto phrDto, @AuthenticationPrincipal UserDetails userDetails) {
        return ResponseEntity.status(HttpStatus.CREATED).body(phrService.createPHR(phrDto, userDetails.getUsername()));
    }

    @CrossOrigin
    @PutMapping("/{id}")
    public ResponseEntity<PhrDto> updatePhr(@PathVariable Long id, @RequestBody PhrDto phrDto) {
        return ResponseEntity.status(HttpStatus.OK).body(phrService.updatePHR(id, phrDto));
    }

    @CrossOrigin
    @GetMapping("/pending")
    public ResponseEntity<List<PhrDto>> getPendingPHRs() {
        return new ResponseEntity<>(phrService.getPendingPHRs(), HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/{id}/accept")
    public ResponseEntity<PhrDto> accept(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(phrService.acceptPHR(id));
    }

    @CrossOrigin
    @PutMapping("/{id}/reject")
    public ResponseEntity<PhrDto> reject(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(phrService.rejectPHR(id));
    }

    @CrossOrigin
    @PutMapping("/{id}/cancel")
    public ResponseEntity<PhrDto> cancel(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(phrService.cancelPHR(id));
    }

    @CrossOrigin
    @PutMapping("/{id}/dispatch-review")
    public ResponseEntity<PhrDto> dispatchReview(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(phrService.dispatchReview(id));
    }

    @CrossOrigin
    @GetMapping("/{id}/comment")
    public ResponseEntity<List<PhrCommentDto>> getPhrCommentsByPhrId(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(phrService.getPhrCommentsByPhrId(id));
    }

    @CrossOrigin
    @PostMapping("/{id}/comment")
    public ResponseEntity<PhrCommentDto> addPhrComment(@PathVariable Long id,
                                                       @RequestBody PhrCommentDto phrCommentDto,
                                                       @AuthenticationPrincipal UserDetails userDetails) {

        return ResponseEntity.status(HttpStatus.OK).body(phrService.addPhrComment(id, phrCommentDto, userDetails.getUsername()));
    }
}
