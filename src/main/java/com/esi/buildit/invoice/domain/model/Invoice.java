package com.esi.buildit.invoice.domain.model;

import com.esi.buildit.common.domain.BaseEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor(force = true, access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of")
@Table(name = "invoice", indexes = {@Index(name = "invoice_idx", columnList = "uuid"), @Index(name = "po_idx", columnList = "po_uuid")})
public class Invoice extends BaseEntity {

    @Column(name = "uuid", nullable = false)
    UUID uuid;
    @Column(name = "po_uuid")
    UUID purchaseOrderUuid;
    InvoiceStatus status;
    String resourceLocation;

    String plantName;
    LocalDate paymentDate;
    BigDecimal totalPrice;
}
