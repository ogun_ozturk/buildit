package com.esi.buildit.invoice.domain.repository;

import com.esi.buildit.invoice.domain.model.Invoice;
import com.esi.buildit.invoice.domain.model.InvoiceStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    Optional<Invoice> findByUuidAndStatus(UUID uuid, InvoiceStatus status);
    Optional<Invoice> findByUuid(UUID uuid);
    Optional<Invoice> findByPurchaseOrderUuidAndStatus(UUID poUuid, InvoiceStatus status);
    Optional<Invoice> findByPurchaseOrderUuid(UUID poUuid);

}
