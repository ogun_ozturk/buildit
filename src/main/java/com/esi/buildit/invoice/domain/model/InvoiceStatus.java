package com.esi.buildit.invoice.domain.model;

public enum InvoiceStatus {
    PENDING,
    REJECTED,
    CLOSED
}
