package com.esi.buildit.invoice.application.service;

import com.esi.buildit.common.application.exception.NotFoundException;
import com.esi.buildit.common.application.exception.RentItException;
import com.esi.buildit.invoice.application.dto.InvoiceDto;
import com.esi.buildit.invoice.application.exception.ExistingInvoiceException;
import com.esi.buildit.invoice.application.exception.InvoiceIllegalStateException;
import com.esi.buildit.invoice.domain.model.Invoice;
import com.esi.buildit.invoice.domain.model.InvoiceStatus;
import com.esi.buildit.invoice.domain.repository.InvoiceRepository;
import com.esi.buildit.phr.domain.model.PhrStatus;
import com.esi.buildit.phr.domain.model.PlantHireRequest;
import com.esi.buildit.phr.domain.repository.PhrRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class InvoiceService {

    @Autowired
    PhrRepository phrRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    InvoiceAssembler invoiceAssembler;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ObjectMapper objectMapper;

    public InvoiceDto processInvoice(InvoiceDto dto) {
        UUID uuid = UUID.fromString(dto.getPurchaseOrderUuid());
        Optional<PlantHireRequest> phr = phrRepository.findByPurchaseOrderUuid(uuid);

        if (!phr.isPresent()) throw new NotFoundException("Such purchase order is not registered in BuildIT system");

        // add invoice to somewhere
        UUID uuid2 = UUID.fromString(dto.getInvoiceUuid());
        Optional<Invoice> existing = invoiceRepository.findByUuid(uuid2);

        if (existing.isPresent()) throw new ExistingInvoiceException("Identical invoice is present");
        existing = invoiceRepository.findByPurchaseOrderUuid(uuid);

        Invoice invoice = Invoice.of(
                uuid2, uuid, InvoiceStatus.PENDING,
                dto.getLink("self").getHref(), // get from hatoas link
                dto.getPlantName(),
                dto.getPaymentDate(),
                dto.getTotalPrice()
        );

        if (existing.isPresent()) {
            InvoiceStatus status = existing.get().getStatus();
            if (status != InvoiceStatus.REJECTED) throw new ExistingInvoiceException("There is already one active invoice");
            invoiceRepository.delete(existing.get());
        }

        invoiceRepository.save(invoice);
        return invoiceAssembler.toResource(invoice);
    }

    public InvoiceDto accept(Long id) {
        Invoice invoice = getInvoiceById(id);
        InvoiceDto dto = getInvoiceDtoFromRentITWithCertainStatus(invoice, InvoiceStatus.PENDING);
        Link accept = dto.getLink("acceptInvoice");

        ResponseEntity<InvoiceDto> response = makeRentITRequest(accept.getHref(), HttpMethod.PUT);
        dto = response.getBody();
        if (response.getStatusCode() != HttpStatus.OK || dto == null) throw new RentItException("Cannot accept invoice in RentIT system", response.getStatusCode());

        // acceptance was successful
        invoice.setStatus(InvoiceStatus.CLOSED);
        invoiceRepository.save(invoice);

        Optional<PlantHireRequest> phrOpt = phrRepository.findByPurchaseOrderUuid(invoice.getPurchaseOrderUuid());
        if (phrOpt.isPresent()) {
            PlantHireRequest phr = phrOpt.get();
            phr.setStatus(PhrStatus.CLOSED);
            phrRepository.save(phr);
        }
        return invoiceAssembler.toResource(invoice);
    }

    public InvoiceDto reject(Long id) {
        Invoice invoice = getInvoiceById(id);
        InvoiceDto dto = getInvoiceDtoFromRentITWithCertainStatus(invoice, InvoiceStatus.PENDING);
        Link reject = dto.getLink("rejectInvoice");

        ResponseEntity<InvoiceDto> response = makeRentITRequest(reject.getHref(), HttpMethod.PUT);
        dto = response.getBody();
        if (response.getStatusCode() != HttpStatus.OK || dto == null) throw new RentItException("Cannot reject invoice in RentIT system", response.getStatusCode());

        // rejection was successful
        invoice.setStatus(InvoiceStatus.REJECTED);
        invoiceRepository.save(invoice);
        return invoiceAssembler.toResource(invoice);
    }

    public List<InvoiceDto> fetchAllInvoices() {
        List<Invoice> invoices = invoiceRepository.findAll();
        return invoiceAssembler.toResources(invoices);
    }

    private ResponseEntity<InvoiceDto> makeRentITRequest(String url, HttpMethod method) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaTypes.HAL_JSON_UTF8);
        return restTemplate.exchange(
                url,
                method,
                new HttpEntity<>(null, headers),
                InvoiceDto.class);
    }

    private InvoiceDto getInvoiceDtoFromRentITWithCertainStatus(Invoice invoice, InvoiceStatus wantedStatus) {
        if (invoice.getStatus() != wantedStatus) throw new InvoiceIllegalStateException("Invoice must be in PENDING state");

        ResponseEntity<InvoiceDto> response = makeRentITRequest(invoice.getResourceLocation(), HttpMethod.GET);
        InvoiceDto dto = response.getBody();
        if (response.getStatusCode() != HttpStatus.OK || dto == null) throw new RentItException("Cannot get such invoice from RentIT", response.getStatusCode());
        return dto;
    }

    private Invoice getInvoiceById(Long id) {
        Optional<Invoice> invoice = invoiceRepository.findById(id);
        if (!invoice.isPresent()) throw new NotFoundException("Invoice not found from buildIT system");
        return invoice.get();
    }
}
