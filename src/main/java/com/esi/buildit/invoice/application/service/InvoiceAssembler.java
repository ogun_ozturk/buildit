package com.esi.buildit.invoice.application.service;

import com.esi.buildit.invoice.application.dto.InvoiceDto;
import com.esi.buildit.invoice.domain.model.Invoice;
import com.esi.buildit.invoice.domain.model.InvoiceStatus;
import com.esi.buildit.invoice.rest.InvoiceController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class InvoiceAssembler extends ResourceAssemblerSupport<Invoice, InvoiceDto> {

    public InvoiceAssembler() {
        super(InvoiceController.class, InvoiceDto.class);
    }

    @Override
    public InvoiceDto toResource(Invoice invoice) {
        InvoiceDto dto = createResourceWithId(invoice.getId(), invoice);

        dto.set_id(invoice.getId());
        dto.setInvoiceUuid(invoice.getUuid().toString());
        dto.setPurchaseOrderUuid(invoice.getPurchaseOrderUuid().toString());
        dto.setStatus(invoice.getStatus().name());
        dto.setPlantName(invoice.getPlantName());
        dto.setPaymentDate(invoice.getPaymentDate());
        dto.setTotalPrice(invoice.getTotalPrice());

        InvoiceStatus status = invoice.getStatus();
        switch (status) {
            case PENDING:
                dto.add(linkTo(methodOn(InvoiceController.class).accept(dto.get_id())).withRel("accept"));
                dto.add(linkTo(methodOn(InvoiceController.class).reject(dto.get_id())).withRel("reject"));
                break;
            case REJECTED:
                // no operations
                break;
        }
        return dto;
    }
}
