package com.esi.buildit.invoice.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;
import java.time.LocalDate;

@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
public class InvoiceDto extends ResourceSupport {

    private Long _id;
    private String invoiceUuid;
    private String purchaseOrderUuid;
    private String status;
    private String plantName;
    private LocalDate paymentDate;
    private BigDecimal totalPrice;

}
