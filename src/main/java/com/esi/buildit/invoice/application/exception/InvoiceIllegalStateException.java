package com.esi.buildit.invoice.application.exception;

public class InvoiceIllegalStateException extends RuntimeException {
    public InvoiceIllegalStateException(String message) {
        super(message);
    }
}
