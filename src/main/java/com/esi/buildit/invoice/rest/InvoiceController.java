package com.esi.buildit.invoice.rest;

import com.esi.buildit.invoice.application.dto.InvoiceDto;
import com.esi.buildit.invoice.application.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceService service;

    @CrossOrigin
    @GetMapping
    public ResponseEntity<List<InvoiceDto>> fetchInvoices() {
        return ResponseEntity.status(HttpStatus.OK).body(service.fetchAllInvoices());
    }

    @CrossOrigin
    @PostMapping
    public ResponseEntity<InvoiceDto> processInvoice(@RequestBody InvoiceDto dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.processInvoice(dto));
    }

    @CrossOrigin
    @PutMapping("/{id}/accept")
    public ResponseEntity<InvoiceDto> accept(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(service.accept(id));
    }

    @CrossOrigin
    @PutMapping("/{id}/reject")
    public ResponseEntity<InvoiceDto> reject(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(service.reject(id));
    }

}
