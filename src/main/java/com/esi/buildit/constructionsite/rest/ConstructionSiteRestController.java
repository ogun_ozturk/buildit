package com.esi.buildit.constructionsite.rest;

import com.esi.buildit.constructionsite.application.dto.ConstructionSiteDto;
import com.esi.buildit.constructionsite.application.service.ConstructionSiteService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/api/v1/construction-site")
@RestController
public class ConstructionSiteRestController {

    private final ConstructionSiteService constructionSiteService;

    public ConstructionSiteRestController(ConstructionSiteService constructionSiteService) {
        this.constructionSiteService = constructionSiteService;
    }

    @CrossOrigin
    @GetMapping
    public List<ConstructionSiteDto> getConstructionSites() {
        return constructionSiteService.getConstructionSites();
    }
}
