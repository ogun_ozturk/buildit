package com.esi.buildit.constructionsite.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
public class ConstructionSiteDto {
    private Long id;
    private String name;
}
