package com.esi.buildit.constructionsite.application.service;

import com.esi.buildit.constructionsite.application.dto.ConstructionSiteDto;
import com.esi.buildit.constructionsite.domain.model.ConstructionSite;
import com.esi.buildit.constructionsite.domain.repository.ConstructionSiteRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ConstructionSiteService {

    private final ConstructionSiteRepository constructionSiteRepository;
    private final ModelMapper modelMapper;

    public ConstructionSiteService(ConstructionSiteRepository constructionSiteRepository, ModelMapper modelMapper) {
        this.constructionSiteRepository = constructionSiteRepository;
        this.modelMapper = modelMapper;
    }

    public List<ConstructionSiteDto> getConstructionSites() {
        List<ConstructionSite> constructionSites = constructionSiteRepository.findAll();

        return constructionSites.stream()
                .map(this::convertToConstructionSiteDto)
                .collect(Collectors.toList());
    }

    private ConstructionSiteDto convertToConstructionSiteDto(ConstructionSite constructionSite) {
        return modelMapper.map(constructionSite, ConstructionSiteDto.class);
    }
}
