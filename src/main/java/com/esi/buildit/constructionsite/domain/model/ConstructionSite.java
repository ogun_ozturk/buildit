package com.esi.buildit.constructionsite.domain.model;

import com.esi.buildit.auth.domain.model.BuildItUser;
import com.esi.buildit.common.domain.BaseEntity;
import com.esi.buildit.phr.domain.model.PlantHireRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
@Entity
public class ConstructionSite extends BaseEntity {

    @Column(nullable = false)
    private String name;

    private String address;

    @OneToMany(mappedBy = "constructionSite", cascade = CascadeType.ALL)
    private List<PlantHireRequest> plantHireRequests;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "construction_site_users", joinColumns = {
            @JoinColumn(name = "construction_site_id")}, inverseJoinColumns = {
            @JoinColumn(name = "user_id")})
    private Set<BuildItUser> constructionSiteEngineers;
}
