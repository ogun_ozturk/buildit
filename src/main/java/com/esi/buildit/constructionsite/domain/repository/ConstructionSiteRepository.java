package com.esi.buildit.constructionsite.domain.repository;

import com.esi.buildit.constructionsite.domain.model.ConstructionSite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConstructionSiteRepository extends JpaRepository<ConstructionSite, Long> {
}
