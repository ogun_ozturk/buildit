package com.esi.buildit.common.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RentItException extends RuntimeException {

    private HttpStatus rentItResponseStatus;

    public RentItException(String message, HttpStatus rentItResponseStatus) {
        super(message);
        this.rentItResponseStatus = rentItResponseStatus;
    }

    public HttpStatus getRentItResponseStatus() {
        return rentItResponseStatus;
    }
}
