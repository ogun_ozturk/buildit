package com.esi.buildit.common.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Embeddable;
import java.time.LocalDate;

@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Value
@Embeddable
public class BusinessPeriod {
    private LocalDate startDate;
    private LocalDate endDate;
}
