package com.esi.buildit.common.rest;

import com.esi.buildit.auth.application.exception.InvalidJwtAuthenticationException;
import com.esi.buildit.common.application.dto.ErrorDetailDto;
import com.esi.buildit.common.application.exception.InvalidBusinessPeriodException;
import com.esi.buildit.common.application.exception.NotFoundException;
import com.esi.buildit.common.application.exception.RentItException;
import com.esi.buildit.invoice.application.exception.ExistingInvoiceException;
import com.esi.buildit.invoice.application.exception.InvoiceIllegalStateException;
import com.esi.buildit.phr.application.exception.InvalidUpdateRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(CustomExceptionHandler.class);

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<ErrorDetailDto> handlePoNotFoundException(NotFoundException ex, WebRequest request) {
        return returnResponseEntity(ex, request, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidBusinessPeriodException.class)
    public final ResponseEntity<ErrorDetailDto> handleInvalidBusinessPeriodException(InvalidBusinessPeriodException ex, WebRequest request) {
        return returnResponseEntity(ex, request, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(InvalidUpdateRequestException.class)
    public final ResponseEntity<ErrorDetailDto> handleInvalidUpdateRequestException(InvalidUpdateRequestException ex, WebRequest request) {
        return returnResponseEntity(ex, request, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(RentItException.class)
    public final ResponseEntity<ErrorDetailDto> handleRentItException(RentItException ex, WebRequest request) {
        return returnResponseEntity(ex, request, ex.getRentItResponseStatus(), ex.getRentItResponseStatus());
    }

    @ExceptionHandler(ExistingInvoiceException.class)
    public final ResponseEntity<ErrorDetailDto> handleExistingInvoiceException(ExistingInvoiceException ex, WebRequest request) {
        return returnResponseEntity(ex, request, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvoiceIllegalStateException.class)
    public final ResponseEntity<ErrorDetailDto> handleInvoiceIllegalStateException(InvoiceIllegalStateException ex, WebRequest request) {
        return returnResponseEntity(ex, request, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleInternalError(Exception ex, WebRequest request) {

        logger.error(ex.getMessage(), ex);

        ErrorDetailDto errorDetailDto = ErrorDetailDto.of(
                LocalDateTime.now(),
                "Ups!Something went wrong",
                request.getDescription(false)
        );

        return new ResponseEntity<>(errorDetailDto, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InvalidJwtAuthenticationException.class)
    public ResponseEntity<Object> handleInvalidJwtAuthenticationException(InvalidJwtAuthenticationException ex, WebRequest request) {

        logger.error(ex.getMessage(), ex);

        ErrorDetailDto errorDetailDto = ErrorDetailDto.of(
                LocalDateTime.now(),
                ex.getUserMessage(),
                request.getDescription(false)
        );

        return new ResponseEntity<>(errorDetailDto, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<ErrorDetailDto> returnResponseEntity(RuntimeException ex,
                                                                WebRequest request,
                                                                HttpStatus httpStatus) {
        logger.error(ex.getMessage(), ex);

        return createResponseEntity(ex, request, httpStatus);
    }

    private ResponseEntity<ErrorDetailDto> returnResponseEntity(RuntimeException ex,
                                                                WebRequest request,
                                                                HttpStatus httpStatus,
                                                                HttpStatus rentItHttpStatus) {

        logger.error("RentIT returned response with HTTP status: " + rentItHttpStatus +
                        "\nException: " + ex.getMessage(),
                ex
        );

        return createResponseEntity(ex, request, httpStatus);
    }

    private ResponseEntity<ErrorDetailDto> createResponseEntity(RuntimeException ex,
                                                                WebRequest request,
                                                                HttpStatus httpStatus) {
        ErrorDetailDto errorDetailDto = ErrorDetailDto.of(
                LocalDateTime.now(),
                ex.getMessage(),
                request.getDescription(false)
        );

        return new ResponseEntity<>(errorDetailDto, httpStatus);
    }
}
