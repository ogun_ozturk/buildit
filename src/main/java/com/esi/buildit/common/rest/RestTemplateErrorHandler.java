package com.esi.buildit.common.rest;

import com.esi.buildit.common.application.exception.RentItException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

public class RestTemplateErrorHandler implements ResponseErrorHandler {

    private Logger logger = LoggerFactory.getLogger(RestTemplateErrorHandler.class);

    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
        return (
                httpResponse.getStatusCode().series() == CLIENT_ERROR ||
                        httpResponse.getStatusCode().series() == SERVER_ERROR
        );
    }

    @Override
    public void handleError(ClientHttpResponse httpResponse) throws IOException {
        if (httpResponse.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR) {
            printLog(HttpStatus.INTERNAL_SERVER_ERROR, true);

            throw new RentItException("RentIT is not available now. please try again later",
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        } else if (httpResponse.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR) {
            if (httpResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
                printLog(HttpStatus.NOT_FOUND, false);

                throw new RentItException("Unable to find corresponding data in RentIT. Please make that sure you provide valid data",
                        HttpStatus.UNPROCESSABLE_ENTITY
                );
            }
            if (httpResponse.getStatusCode() == HttpStatus.UNPROCESSABLE_ENTITY) {
                printLog(HttpStatus.UNPROCESSABLE_ENTITY, false);

                throw new RentItException("Unable to process your data. Please make that sure you provide valid data",
                        HttpStatus.UNPROCESSABLE_ENTITY
                );
            }
        }
    }

    private void printLog(HttpStatus httpStatus, boolean isServerError) {
        if (isServerError) {
            logger.error("Server error occurred while trying to send the request to RentIT. Http response status: " + httpStatus);
        } else {
            logger.error("Client error occurred while trying to send the request to RentIT. Http response status: " + httpStatus);
        }
    }
}
