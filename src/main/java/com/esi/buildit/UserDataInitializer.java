package com.esi.buildit;

import com.esi.buildit.auth.domain.model.BuildItUser;
import com.esi.buildit.auth.domain.model.Role;
import com.esi.buildit.auth.domain.model.RoleType;
import com.esi.buildit.auth.domain.repository.BuildItUserRepository;
import com.esi.buildit.auth.domain.repository.RoleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserDataInitializer implements CommandLineRunner {

    private final BuildItUserRepository buildItUserRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;


    public UserDataInitializer(BuildItUserRepository buildItUserRepository,
                               RoleRepository roleRepository,
                               PasswordEncoder passwordEncoder) {

        this.buildItUserRepository = buildItUserRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {

        Role siteEngineerRole = roleRepository.save(Role.builder().roleType(RoleType.ROLE_SITE_ENGINEER).build());
        Role worksEngineerRole = roleRepository.save(Role.builder().roleType(RoleType.ROLE_WORKS_ENGINEER).build());

        buildItUserRepository.save(
                BuildItUser.builder()
                        .firstName("Berker")
                        .lastName("Demirer")
                        .email("berker.demier@buildit.com")
                        .password(passwordEncoder.encode("pwd123"))
                        .username("berker.d")
                        .role(siteEngineerRole)
                        .build()
        );

        buildItUserRepository.save(
                BuildItUser.builder()
                        .firstName("Tarlan")
                        .lastName("Hasanli")
                        .email("tarlan.hasanli@buildit.com")
                        .password(passwordEncoder.encode("pwd123"))
                        .username("tarlan.h")
                        .role(worksEngineerRole)
                        .build()
        );
    }
}
