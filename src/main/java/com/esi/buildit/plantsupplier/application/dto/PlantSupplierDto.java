package com.esi.buildit.plantsupplier.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
public class PlantSupplierDto {
    private Long id;
    private String name;
    private Long customerId;
    private String catalogueUrl;
    private String availablePlantUrl;
    private String poUrl;
    private String poStatusesUrl;
    private String accessToken;
}
