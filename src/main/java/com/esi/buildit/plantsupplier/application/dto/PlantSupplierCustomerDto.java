package com.esi.buildit.plantsupplier.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
public class PlantSupplierCustomerDto {
    private String plantSupplierName;
    private Long customerIdentifier;
}
