package com.esi.buildit.plantsupplier.application.service;

import com.esi.buildit.common.application.exception.NotFoundException;
import com.esi.buildit.plantsupplier.application.dto.PlantSupplierCustomerDto;
import com.esi.buildit.plantsupplier.application.dto.PlantSupplierDto;
import com.esi.buildit.plantsupplier.application.dto.RentItUrlsDto;
import com.esi.buildit.plantsupplier.domain.model.PlantSupplier;
import com.esi.buildit.plantsupplier.domain.repository.PlantSupplierRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PlantSupplierService {

    private final PlantSupplierRepository plantSupplierRepository;
    private final ModelMapper modelMapper;

    public PlantSupplierService(PlantSupplierRepository plantSupplierRepository, ModelMapper modelMapper) {
        this.plantSupplierRepository = plantSupplierRepository;
        this.modelMapper = modelMapper;
    }

    public List<PlantSupplierDto> getPlantSuppliers() {

        List<PlantSupplier> plantSuppliers = plantSupplierRepository.findAll();

        if (plantSuppliers.isEmpty()) {
            throw new NotFoundException("No plant suppliers found");
        }

        return plantSuppliers.stream()
                .map(this::convertToPlantSupplierDto)
                .collect(Collectors.toList());
    }

    public RentItUrlsDto getPlantSupplierUrls(Long plantSupplierId) {
        Optional<PlantSupplier> optionalPlantSupplier = plantSupplierRepository.findById(plantSupplierId);

        if (!optionalPlantSupplier.isPresent()) {
            throw new NotFoundException("No plant supplier found");
        }

        PlantSupplier plantSupplier = optionalPlantSupplier.get();

        return RentItUrlsDto.of(
                plantSupplier.getCatalogueUrl(),
                plantSupplier.getAvailablePlantUrl(),
                plantSupplier.getPoStatusesUrl()
        );
    }

    public List<PlantSupplierCustomerDto> getPlantSuppliersWithCustomerIdentifiers() {
        List<PlantSupplier> plantSuppliers = plantSupplierRepository.findAll();

        if (plantSuppliers.isEmpty()) {
            throw new NotFoundException("No plant suppliers found");
        }

        return plantSuppliers.stream()
                .map(ps -> PlantSupplierCustomerDto.of(ps.getName(), ps.getCustomerId()))
                .collect(Collectors.toList());
    }

    private PlantSupplierDto convertToPlantSupplierDto(PlantSupplier plantSupplier) {
        return modelMapper.map(plantSupplier, PlantSupplierDto.class);
    }
}
