package com.esi.buildit.plantsupplier.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
public class RentItUrlsDto {
    private String catalogueUrl;
    private String availableUrl;
    private String poStatusesUrl;
}
