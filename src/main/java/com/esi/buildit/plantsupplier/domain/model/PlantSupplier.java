package com.esi.buildit.plantsupplier.domain.model;

import com.esi.buildit.common.domain.BaseEntity;
import com.esi.buildit.phr.domain.model.PlantHireRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Data
@Entity
public class PlantSupplier extends BaseEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Long customerId; // BuildIt user identifier in RentIt side

    @Column(nullable = false)
    private String catalogueUrl;

    @Column(nullable = false)
    private String availablePlantUrl;

    @Column(nullable = false)
    private String poUrl;

    @Column(nullable = false)
    private String poStatusesUrl;

    @Column(nullable = false)
    private String itemTotalCostUrl;

    @Column(nullable = false)
    private String accessToken;

    @OneToMany(mappedBy = "plantSupplier", cascade = CascadeType.ALL)
    private List<PlantHireRequest> plantHireRequests;
}
