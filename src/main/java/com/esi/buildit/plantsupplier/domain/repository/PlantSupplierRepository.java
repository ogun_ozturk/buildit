package com.esi.buildit.plantsupplier.domain.repository;

import com.esi.buildit.plantsupplier.domain.model.PlantSupplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlantSupplierRepository extends JpaRepository<PlantSupplier, Long> {
}
