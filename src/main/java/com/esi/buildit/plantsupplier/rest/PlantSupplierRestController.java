package com.esi.buildit.plantsupplier.rest;

import com.esi.buildit.plantsupplier.application.dto.PlantSupplierCustomerDto;
import com.esi.buildit.plantsupplier.application.dto.PlantSupplierDto;
import com.esi.buildit.plantsupplier.application.dto.RentItUrlsDto;
import com.esi.buildit.plantsupplier.application.service.PlantSupplierService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/plant-supplier")
public class PlantSupplierRestController {

    private final PlantSupplierService plantSupplierService;

    public PlantSupplierRestController(PlantSupplierService plantSupplierService) {
        this.plantSupplierService = plantSupplierService;
    }

    @CrossOrigin
    @GetMapping
    public List<PlantSupplierDto> getPlantSuppliers() {
        return plantSupplierService.getPlantSuppliers();
    }

    @CrossOrigin
    @GetMapping("/{id}/urls")
    public RentItUrlsDto getPlantSupplierUrls(@PathVariable Long id) {
        return plantSupplierService.getPlantSupplierUrls(id);
    }

    @CrossOrigin
    @GetMapping("/customers") //TODO find a better name for the url
    public List<PlantSupplierCustomerDto> getPlantSuppliersWithCustomerIdentifiers() {
        return plantSupplierService.getPlantSuppliersWithCustomerIdentifiers();
    }
}
