Feature: Creation of PHR
  As a site engineer
  I want to create Plant Hire Request

  Background: Plant Supplier
    When the user is in the "Build It Login" Web page
    And I login to the system with username "berker.d" and password "pwd123"
    And user goes to create phr page

  Scenario: Creating Plant Hire Request
    When the user selects supplier "Our RentIt"
    And the user selects "Mini excavator Yanmar 31NV70 300.9D"
    And the user queries the plant catalog available from "05/24/2019" to "06/17/2019"
    And the user submit the query
    And the user selects first plant from list
    And the user selects Construction Site "Dope Construction Site"
    And Create PHR
    Then a PHR should be created

  Scenario: Creating Plant Hire Request with error
    When the user selects supplier "Our RentIt"
    And the user selects "Mini excavator Yanmar 31NV70 300.9D"
    And the user queries the plant catalog available from "05/14/2019" to "05/22/2019"
    And the user submit the query
    Then No available plants found