Feature: Cancellation of PHR
  As a site engineer
  I want to cancel and edit Plant Hire Request

  Background:
    When the user is in the "Build It Login" Web page
    And I login to the system with username "berker.d" and password "pwd123"
    And user go to Review PHR web page

  Scenario: Edit Phr
    When user edit first phr in the list
    And the user selects Construction Site "Bad Construction Site"
    And user enters new date from "05/25/2019" to "06/18/2019"
    Then user edit phr

  Scenario: Cancel PHR
    When user cancel first phr in the list
    Then the status is "CANCELED_BY_SITE_ENGINEER"
