-- INSERT INTO role (role_type, created_at, updated_at)
-- VALUES ('ROLE_SITE_ENGINEER', current_timestamp, current_timestamp),
--        ('ROLE_WORKS_ENGINEER', current_timestamp, current_timestamp);
--
-- INSERT INTO build_it_user (email, first_name, last_name, password, username, role_id, created_At, updated_at)
-- VALUES ('tarlan.hasanli@plantit.com', 'Tarlan', 'Hasanli', 'pwd123', 'tarlan.h', 1, current_timestamp,
--         current_timestamp),
--        ('berker.demirer@plantit.com', 'Berker', 'Demirer', 'pwd123', 'berker.d', 2, current_timestamp,
--         current_timestamp);

INSERT INTO construction_site (name, address, created_at, updated_at)
VALUES ('Dope Construction Site', 'Dope Construction Site Address', current_timestamp, current_timestamp),
       ('Bad Construction Site', 'Bad Construction Site Address', current_timestamp, current_timestamp);

INSERT INTO plant_supplier (name,
                            customer_id,
                            access_token,
                            catalogue_url,
                            available_plant_url,
                            po_url,
                            po_statuses_url,
                            item_total_cost_url,
                            created_at,
                            updated_at)
VALUES ('Our RentIt',
        1,
        'token-will-be-here',
        'https://stark-mountain-22778.herokuapp.com/api/v1/inventory',
        'https://stark-mountain-22778.herokuapp.com/api/v1/inventory/available',
        'https://stark-mountain-22778.herokuapp.com/api/v1/purchase-order',
        'https://stark-mountain-22778.herokuapp.com/api/v1/customer-orders',
        'https://stark-mountain-22778.herokuapp.com/api/v1/item-total-price',
        current_timestamp,
        current_timestamp);