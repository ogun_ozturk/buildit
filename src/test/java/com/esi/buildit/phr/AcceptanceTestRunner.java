package com.esi.buildit.phr;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin={"pretty","html:target/cucumber"},
		features="classpath:features",
		glue="com.esi.buildit.phr")
public class AcceptanceTestRunner {
}
