package com.esi.buildit.phr;

import com.esi.buildit.BuildItApplication;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

@ContextConfiguration(classes = BuildItApplication.class)
@WebAppConfiguration
public class CreationOfPHRSteps {

	private static final String LOGIN_URL = "http://localhost:4200/login";
	private WebDriverWait wait;
	private WebDriver driver;

	@Before  // Use `Before` from Cucumber library
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 30);
	}

	@After  // Use `After` from Cucumber library
	public void tearOff() {
		driver.close();
	}

	@When("^the user is in the \"([^\"]*)\" Web page$")
	public void the_user_is_in_login_the_Web_page(String arg1) throws Throwable {
		driver.navigate().to(LOGIN_URL);
	}

	@And("^I login to the system with username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void login(String username, String password) throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='text']")));
		WebElement uname = driver.findElement(By.xpath("//input[@type='text']"));
		WebElement pass = driver.findElement(By.xpath("//input[@type='password']"));
		WebElement btn = driver.findElement(By.tagName("button"));

		uname.sendKeys(username);
		pass.sendKeys(password);
		btn.click();
	}

	@And("^user goes to create phr page$")
	public void user_goes_to_create_phr_page() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='/phr']"))).click();
	}

	@When("^the user selects supplier \"([^\"]*)\"$")
	public void the_user_selects_supplier(String supplierName) throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.id("suppliers"))).sendKeys(supplierName);
	}

	@And ("^the user selects \"([^\"]*)\"$")
	public void the_user_selects(String arg1) throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format("//tr[./td = '%s']//button", arg1)))).click();
	}

	@And("^the user queries the plant catalog available from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void the_user_queries_the_plant_catalog_available_from_to(String startDate, String endDate) throws Throwable {
		WebElement availableDates = driver.findElement(By.id("availableDates"));
		availableDates.sendKeys(startDate + " - " + endDate);
		availableDates.sendKeys(Keys.TAB);
	}

	@And("^the user submit the query$")
	public void the_user_submit_the_query() throws Throwable {
		driver.findElement(By.xpath("//button[contains(text(),'" + "Find Available Plant Items" + "')]")).click();
	}

	@And("^the user selects first plant from list$")
	public void the_user_selects_first_plant_from_list() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("btn-success"))).click();
	}

	@And("^the user selects Construction Site \"([^\"]*)\"$")
	public void the_user_selects_Construction_Site(String param) throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("constructionSiteId"))).sendKeys(param);
	}

	@And("^Create PHR$")
	public void create_PHR() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("btn-block"))).click();
	}

	@Then("^a PHR should be created$")
	public void a_PHR_should_be_created() throws Throwable {
		driver.getPageSource().contains("PHR Created Successfuly");
	}

	@Then("^No available plants found$")
	public void no_available_plants_found() throws Throwable {
		driver.getPageSource().contains("No Available Plants Found For This Entry!");
	}

	@And("^user go to Review PHR web page$")
	public void user_go_to_web_page() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@href='/review-phr-site']"))).click();
	}

	@When("^user cancel first phr in the list$")
	public void user_cancel_phr() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Cancel')]"))).click();
	}

	@Then("^the status is \"([^\"]*)\"$")
	public void the_status_is(String param) throws Throwable {
		driver.getPageSource().contains(param);
	}

	@When("^user edit first phr in the list$")
	public void user_edit_first_phr_in_the_list() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Edit')]"))).click();
	}

	@And("^user enters new date from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void user_enters_new_date_from(String arg1, String arg2) throws Throwable {
		WebElement hirePeriod = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("hirePeriod")));
		hirePeriod.sendKeys(arg1 + " - " + arg2);
		hirePeriod.sendKeys(Keys.TAB);
	}

	@Then("^user edit phr$")
	public void user_edit_phr() throws Throwable {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Modify PHR')]"))).click();
	}

}
