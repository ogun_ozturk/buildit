package com.esi.buildit.rest;

import com.esi.buildit.BuildItApplication;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BuildItApplication.class)
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PHRRestControllerTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Autowired
	ObjectMapper mapper;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void a_getPHRs() throws Exception{

		mockMvc.perform(get("/api/v1/phr")
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();

	}

	@Test
	public void b_createPhr() throws Exception{

		mockMvc.perform(post("/api/v1/phr")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"_id\":1,\"cancelPoUrl\":\"Sample URL\",\"constructionSiteId\":1,\"totalCost\":100,\"phrCommentDtoList\":[{}],\"plantItemIdentifier\":1,\"plantSupplierId\":1,\"relevantPoUrl\":\"Sample URL\",\"rentalPeriod\":{\"endDate\":\"2019-05-28\",\"startDate\":\"2019-05-20\"},\"siteEngineerId\":1,\"status\":\"PENDING_FOR_WORKS_ENGINEER_APPROVAL\"}"))
				.andDo(print())
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();

	}

	@Test
	public void c_updatePhr() throws Exception{

		mockMvc.perform(put("/api/v1/phr/{id}", 1)
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"_id\":1,\"cancelPoUrl\":\"Sample URL\",\"constructionSiteId\":1,\"totalCost\":100,\"phrCommentDtoList\":[{}],\"plantItemIdentifier\":1,\"plantSupplierId\":1,\"relevantPoUrl\":\"Sample URL\",\"rentalPeriod\":{\"endDate\":\"2019-05-28\",\"startDate\":\"2019-05-20\"},\"siteEngineerId\":1,\"status\":\"PENDING_FOR_WORKS_ENGINEER_APPROVAL\"}"))
				.andDo(print())
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();

	}

	@Test
	public void d_acceptPhr() throws Exception{

		mockMvc.perform(post("/api/v1/phr/{id}/accept", 1)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

	}

	@Test
	public void e_rejectPhr() throws Exception{

		mockMvc.perform(post("/api/v1/phr/{id}/reject", 1)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

	}

	@Test
	public void f_getPendingPHRs() throws Exception{

		mockMvc.perform(get("/api/v1/phr/pending")
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();

	}



}
