$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/creation_of_phr.feature");
formatter.feature({
  "line": 1,
  "name": "Creation of PHR",
  "description": "As a site engineer\r\nI want to create Plant Hire Request",
  "id": "creation-of-phr",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3606463706,
  "status": "passed"
});
formatter.background({
  "line": 5,
  "name": "Plant Supplier",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 6,
  "name": "the user is in the \"Build It Login\" Web page",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I login to the system with username \"berker.d\" and password \"pwd123\"",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user goes to create phr page",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Build It Login",
      "offset": 20
    }
  ],
  "location": "CreationOfPHRSteps.the_user_is_in_login_the_Web_page(String)"
});
formatter.result({
  "duration": 4194134195,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "berker.d",
      "offset": 37
    },
    {
      "val": "pwd123",
      "offset": 61
    }
  ],
  "location": "CreationOfPHRSteps.login(String,String)"
});
formatter.result({
  "duration": 664331002,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPHRSteps.user_goes_to_create_phr_page()"
});
formatter.result({
  "duration": 762393085,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Creating Plant Hire Request",
  "description": "",
  "id": "creation-of-phr;creating-plant-hire-request",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 11,
  "name": "the user selects supplier \"Our RentIt\"",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "the user selects \"Mini excavator Yanmar 31NV70 300.9D\"",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "the user queries the plant catalog available from \"05/24/2019\" to \"06/17/2019\"",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "the user submit the query",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "the user selects first plant from list",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "the user selects Construction Site \"Dope Construction Site\"",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "Create PHR",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "a PHR should be created",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Our RentIt",
      "offset": 27
    }
  ],
  "location": "CreationOfPHRSteps.the_user_selects_supplier(String)"
});
formatter.result({
  "duration": 206981963,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Mini excavator Yanmar 31NV70 300.9D",
      "offset": 18
    }
  ],
  "location": "CreationOfPHRSteps.the_user_selects(String)"
});
formatter.result({
  "duration": 188557385,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "05/24/2019",
      "offset": 51
    },
    {
      "val": "06/17/2019",
      "offset": 67
    }
  ],
  "location": "CreationOfPHRSteps.the_user_queries_the_plant_catalog_available_from_to(String,String)"
});
formatter.result({
  "duration": 381918461,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPHRSteps.the_user_submit_the_query()"
});
formatter.result({
  "duration": 117686446,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPHRSteps.the_user_selects_first_plant_from_list()"
});
formatter.result({
  "duration": 643004526,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dope Construction Site",
      "offset": 36
    }
  ],
  "location": "CreationOfPHRSteps.the_user_selects_Construction_Site(String)"
});
formatter.result({
  "duration": 348107168,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPHRSteps.create_PHR()"
});
formatter.result({
  "duration": 241065186,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPHRSteps.a_PHR_should_be_created()"
});
formatter.result({
  "duration": 38541599,
  "status": "passed"
});
formatter.after({
  "duration": 138561794,
  "status": "passed"
});
formatter.before({
  "duration": 2862120303,
  "status": "passed"
});
formatter.background({
  "line": 5,
  "name": "Plant Supplier",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 6,
  "name": "the user is in the \"Build It Login\" Web page",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I login to the system with username \"berker.d\" and password \"pwd123\"",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user goes to create phr page",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Build It Login",
      "offset": 20
    }
  ],
  "location": "CreationOfPHRSteps.the_user_is_in_login_the_Web_page(String)"
});
formatter.result({
  "duration": 3934745693,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "berker.d",
      "offset": 37
    },
    {
      "val": "pwd123",
      "offset": 61
    }
  ],
  "location": "CreationOfPHRSteps.login(String,String)"
});
formatter.result({
  "duration": 648749727,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPHRSteps.user_goes_to_create_phr_page()"
});
formatter.result({
  "duration": 851936637,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Creating Plant Hire Request with error",
  "description": "",
  "id": "creation-of-phr;creating-plant-hire-request-with-error",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 21,
  "name": "the user selects supplier \"Our RentIt\"",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "the user selects \"Mini excavator Yanmar 31NV70 300.9D\"",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "the user queries the plant catalog available from \"05/14/2019\" to \"05/22/2019\"",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "the user submit the query",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "No available plants found",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Our RentIt",
      "offset": 27
    }
  ],
  "location": "CreationOfPHRSteps.the_user_selects_supplier(String)"
});
formatter.result({
  "duration": 205001095,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Mini excavator Yanmar 31NV70 300.9D",
      "offset": 18
    }
  ],
  "location": "CreationOfPHRSteps.the_user_selects(String)"
});
formatter.result({
  "duration": 229275549,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "05/14/2019",
      "offset": 51
    },
    {
      "val": "05/22/2019",
      "offset": 67
    }
  ],
  "location": "CreationOfPHRSteps.the_user_queries_the_plant_catalog_available_from_to(String,String)"
});
formatter.result({
  "duration": 823819912,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPHRSteps.the_user_submit_the_query()"
});
formatter.result({
  "duration": 178559177,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPHRSteps.no_available_plants_found()"
});
formatter.result({
  "duration": 14998165,
  "status": "passed"
});
formatter.after({
  "duration": 138449723,
  "status": "passed"
});
formatter.uri("features/phr_cancel_edit.feature");
formatter.feature({
  "line": 1,
  "name": "Cancellation of PHR",
  "description": "As a site engineer\r\nI want to cancel and edit Plant Hire Request",
  "id": "cancellation-of-phr",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2882091119,
  "status": "passed"
});
formatter.background({
  "line": 5,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 6,
  "name": "the user is in the \"Build It Login\" Web page",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I login to the system with username \"berker.d\" and password \"pwd123\"",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user go to Review PHR web page",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Build It Login",
      "offset": 20
    }
  ],
  "location": "CreationOfPHRSteps.the_user_is_in_login_the_Web_page(String)"
});
formatter.result({
  "duration": 3563260639,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "berker.d",
      "offset": 37
    },
    {
      "val": "pwd123",
      "offset": 61
    }
  ],
  "location": "CreationOfPHRSteps.login(String,String)"
});
formatter.result({
  "duration": 466715212,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPHRSteps.user_go_to_web_page()"
});
formatter.result({
  "duration": 711516286,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Edit Phr",
  "description": "",
  "id": "cancellation-of-phr;edit-phr",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 11,
  "name": "user edit first phr in the list",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "the user selects Construction Site \"Bad Construction Site\"",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "user enters new date from \"05/25/2019\" to \"06/18/2019\"",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "user edit phr",
  "keyword": "Then "
});
formatter.match({
  "location": "CreationOfPHRSteps.user_edit_first_phr_in_the_list()"
});
formatter.result({
  "duration": 772239400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bad Construction Site",
      "offset": 36
    }
  ],
  "location": "CreationOfPHRSteps.the_user_selects_Construction_Site(String)"
});
formatter.result({
  "duration": 777867978,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "05/25/2019",
      "offset": 27
    },
    {
      "val": "06/18/2019",
      "offset": 43
    }
  ],
  "location": "CreationOfPHRSteps.user_enters_new_date_from(String,String)"
});
formatter.result({
  "duration": 385617372,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPHRSteps.user_edit_phr()"
});
formatter.result({
  "duration": 145261589,
  "status": "passed"
});
formatter.after({
  "duration": 153843265,
  "status": "passed"
});
formatter.before({
  "duration": 2918795209,
  "status": "passed"
});
formatter.background({
  "line": 5,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 6,
  "name": "the user is in the \"Build It Login\" Web page",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I login to the system with username \"berker.d\" and password \"pwd123\"",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user go to Review PHR web page",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Build It Login",
      "offset": 20
    }
  ],
  "location": "CreationOfPHRSteps.the_user_is_in_login_the_Web_page(String)"
});
formatter.result({
  "duration": 4729444225,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "berker.d",
      "offset": 37
    },
    {
      "val": "pwd123",
      "offset": 61
    }
  ],
  "location": "CreationOfPHRSteps.login(String,String)"
});
formatter.result({
  "duration": 797996945,
  "status": "passed"
});
formatter.match({
  "location": "CreationOfPHRSteps.user_go_to_web_page()"
});
formatter.result({
  "duration": 1028663426,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "Cancel PHR",
  "description": "",
  "id": "cancellation-of-phr;cancel-phr",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 17,
  "name": "user cancel first phr in the list",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "the status is \"CANCELED_BY_SITE_ENGINEER\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CreationOfPHRSteps.user_cancel_phr()"
});
formatter.result({
  "duration": 757007990,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CANCELED_BY_SITE_ENGINEER",
      "offset": 15
    }
  ],
  "location": "CreationOfPHRSteps.the_status_is(String)"
});
formatter.result({
  "duration": 20851454,
  "status": "passed"
});
formatter.after({
  "duration": 245418887,
  "status": "passed"
});
});